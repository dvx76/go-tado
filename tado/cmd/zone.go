package cmd

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"
)

func init() {
	zoneCmd.AddCommand(zoneListCmd)
	zoneCmd.AddCommand(zoneStateCmd)
	zoneCmd.AddCommand(zoneOverlayCmd)
	rootCmd.AddCommand(zoneCmd)
}

var (
	zoneCmd = &cobra.Command{
		Use:   "zone",
		Short: "Interact with Zones",
	}

	zoneListCmd = &cobra.Command{
		Use:   "list",
		Short: "List all Zones",
		RunE: func(cmd *cobra.Command, args []string) error {
			zones, err := tadoClient.Zone()
			if err != nil {
				return err
			}
			w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)
			fmt.Fprintln(w, "Id\tName\tType\tDevices")
			for _, v := range *zones {
				fmt.Fprintf(w, "%v\t%v\t%v\t%v\n",
					v.Id,
					v.Name,
					v.Type,
					strings.Join(v.DeviceTypes, ","),
				)
			}
			w.Flush()
			return nil
		},
	}

	zoneStateCmd = &cobra.Command{
		Use:   "state [zone id]",
		Short: "Retrieve the State of a Zone",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			zoneState, err := tadoClient.ZoneState(args[0])
			prettyPrint(zoneState)
			fmt.Printf("Type: %v\n", zoneState.Setting.Type)
			fmt.Printf("Power: %v%%\n", zoneState.ActivityDataPoints.HeatingPower.Percentage)
			fmt.Printf("Set Temp: %vC\n", zoneState.Setting.Temperature.Celsius)
			fmt.Printf("Current Temp: %vC\n", zoneState.SensorDataPoints.InsideTemperature.Celsius)
			fmt.Printf("Humidity: %v%%\n", zoneState.SensorDataPoints.Humidity.Percentage)
			return err
		},
	}

	zoneOverlayCmd = &cobra.Command{
		Use:   "set [zone id] [set temperature] [duration]",
		Short: "Set temperature override for a Zone",
		Long: `Set a temperature override
		[zone id] is the Zone ID (mandatory)
		[set temperature] is the target temperature (mandatory)
		[duration] is the duration, in minutes, for the override. If unspecified the override will last until the next time block in the smart schedule`,
		Args: func(cmd *cobra.Command, args []string) error {
			zoneId, err := strconv.ParseInt(args[0], 10, 32)
			if err != nil {
				return fmt.Errorf("[zone id] must be an integer")
			}

			// zoneMap = map of valid zones, keyed by zone id
			zones, err := tadoClient.Zone()
			if err != nil {
				return err
			}
			var zoneMap = make(map[int]string)
			for _, zone := range *zones {
				zoneMap[zone.Id] = zone.Name
			}

			if _, ok := zoneMap[int(zoneId)]; !ok {
				return fmt.Errorf("[zone id] must be a valid Zone ID")
			}

			_, err = strconv.ParseFloat(args[1], 32)
			if err != nil {
				return fmt.Errorf("[set temp] must be a valid temperature (float)")
			}

			if len(args) > 2 {
				_, err = strconv.ParseInt(args[2], 10, 32)
				if err != nil {
					return fmt.Errorf("[duration] must specify the duration in minutes")
				}
			}

			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			// args[0] = Zone ID
			// args[1] = Target temperature to set
			// args[2] = Duration in minutes, optional
			var durationSeconds int
			if len(args) > 2 {
				durationInt, _ := strconv.Atoi(args[2])
				durationSeconds = durationInt * 60
			}
			_, err := tadoClient.ZoneOverlay(args[0], args[1], durationSeconds)
			return err
		},
	}
)

/*

tado zone set [zone id] [temp] --duration

default: until next timeslot


*/
