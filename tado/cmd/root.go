package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/dvx76/tado"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	tadoClient *tado.Client
	err        error
	rootCmd    = &cobra.Command{
		Use:   "tado",
		Short: "Tado is a CLI to interact with the Tado API",
		Long:  `Complete documentation is available at github.com/dvx76/tado/tado`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			username := viper.GetString("username")
			password := viper.GetString("password")
			if username == "" || password == "" {
				return fmt.Errorf("$TADO_USERNAME and $TADO_PASSWORD variables must be set!")
			}

			tadoClient, err = tado.NewClient(username, password)
			return err
		},
	}
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	rootCmd.PersistentFlags().StringP("username", "u", "", "Your Tado username")
	rootCmd.PersistentFlags().StringP("password", "p", "", "Your Tado password")
	viper.SetEnvPrefix("TADO")
	viper.AutomaticEnv()
	viper.BindPFlags(rootCmd.PersistentFlags())
}

// prettyPrint helps printing structs as formatted json
func prettyPrint(obj interface{}) {
	objJSON, _ := json.MarshalIndent(obj, "", "  ")
	log.Println(string(objJSON))
}
