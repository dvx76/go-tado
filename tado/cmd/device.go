package cmd

import (
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
)

func init() {
	deviceCmd.AddCommand(deviceListCmd)
	rootCmd.AddCommand(deviceCmd)
}

var (
	deviceCmd = &cobra.Command{
		Use:   "device",
		Short: "Interact with devices",
	}
	deviceListCmd = &cobra.Command{
		Use:   "list",
		Short: "List all devices",
		RunE: func(cmd *cobra.Command, args []string) error {
			devices, err := tadoClient.Devices()
			if err != nil {
				return err
			}
			w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)
			fmt.Fprintln(w, "Type\tSerial\tFirmware\tConnected")
			for _, v := range *devices {
				fmt.Fprintf(w, "%v\t%v\t%v\t%v\n",
					v.DeviceType,
					v.SerialNo,
					v.CurrentFwVersion,
					v.ConnectionState.Value)
			}
			w.Flush()
			return nil
		},
	}
)
