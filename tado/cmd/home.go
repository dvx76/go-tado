package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(homeCmd)
}

var homeCmd = &cobra.Command{
	Use:   "home",
	Short: "Retrieve the Home details",
	Run: func(cmd *cobra.Command, args []string) {
		homeDetails, _ := tadoClient.HomeDetails()
		prettyPrint(homeDetails)
	},
}
