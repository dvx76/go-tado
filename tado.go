package tado

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"sort"

	"golang.org/x/oauth2"
)

const (
	ClientSecret = "wZaRN7rpjn3FoNyF5IFuxg9uMzYJcvOoQ8QWiIqS3hfk6gLhVlG57j5YNoZL2Rtc"
	ClientId     = "tado-web-app"
	TokenUrl     = "https://auth.tado.com/oauth/token"
	ApiUrl       = "https://my.tado.com/api/v2"
)

// Client is a Tado API Client
type Client struct {
	username   string
	password   string
	oauth2Conf oauth2.Config
	Client     *http.Client
	HomeId     int
}

type Temperature struct {
	Celsius    float32 `json:"celsius"`
	Fahrenheit float32 `json:"fahrenheit"`
}

type TempSetting struct {
	Type        string      `json:"type"`
	Power       string      `json:"power"`
	Temperature Temperature `json:"temperature"`
}

// Home contains information about a home
type Home struct {
	ID              int    `json:"id"`
	Name            string `json:"name"`
	DateTimeZone    string `json:"dateTimeZone"`
	TemperatureUnit string `json:"temperatureUnit"`
}

// MobileDevice contains information about a mobile device
type MobileDevice struct {
	Name     string `json:"name"`
	Id       int    `json:"id"`
	Settings struct {
		GeoTrackingEnabled bool `json:"geoTrackingEnabled"`
		PushNotifications  struct {
			LowBatteryReminder          bool `json:"lowBatteryReminder"`
			AwayModeReminder            bool `json:"awayModeReminder"`
			HomeModeReminder            bool `json:"homeModeReminder"`
			OpenWindowReminder          bool `json:"openWindowReminder"`
			EnergySavingsReportReminder bool `json:"energySavingsReportReminder"`
			IncidentDetection           bool `json:"incidentDetection"`
		} `json:"pushNotifications"`
	} `json:"settings"`
	Location struct {
		Stale           bool `json:"stale"`
		AtHome          bool `json:"atHome"`
		BearingFromHome struct {
			Degrees float32 `json:"degrees"`
			Radians float32 `json:"radians"`
		} `json:"bearingFromHome"`
		RelativeDistanceFromHomeFence float32 `json:"relativeDistanceFromHomeFence"`
	} `json:"location"`
	DeviceMetadata struct {
		Platform  string `json:"platform"`
		OsVersion string `json:"osVersion"`
		Model     string `json:"model"`
		Locale    string `json:"locale"`
	} `json:"deviceMetadata"`
}

// Account contains the account information and the homesand mobileDevices
// associated with it
type Account struct {
	Name          string         `json:"name"`
	Email         string         `json:"email"`
	Username      string         `json:"username"`
	ID            string         `json:"id"`
	Homes         []Home         `json:"homes"`
	Locale        string         `json:"locale"`
	MobileDevices []MobileDevice `json:"mobileDevices"`
}

type HomeDetails struct {
	ID                         int     `json:"id"`
	Name                       string  `json:"name"`
	DateTimeZone               string  `json:"dateTimeZone"`
	DateCreated                string  `json:"dateCreated"`
	TemperatureUnit            string  `json:"temperatureUnit"`
	Partner                    string  `json:"partner"`
	SimpleSmartScheduleEnabled bool    `json:"simpleSmartScheduleEnabled"`
	AwayRadiusInMeters         float64 `json:"awayRadiusInMeters"`
	InstallationCompleted      bool    `json:"installationCompleted"`
	IncidentDetection          struct {
		Supported bool `json:"supported"`
		Enabled   bool `json:"enabled"`
	} `json:"incidentDetection"`
	AutoAssistFreeTrialEnabled bool     `json:"autoAssistFreeTrialEnabled"`
	Skills                     []string `json:"skills"` // Content unknown, string is a guess
	ChristmasModeEnabled       bool     `json:"christmasModeEnabled"`
	ShowAutoAssistReminders    bool     `json:"showAutoAssistReminders"`
	ContactDetails             struct {
		Name  string `json:"name"`
		Email string `json:"email"`
		Phone string `json:"phone"`
	} `json:"contactDetails"`
	Address struct {
		AddressLine1 string `json:"addressLine1"` //
		AddressLine2 string `json:"addressLine2"`
		ZipCode      string `json:"zipCode"`
		City         string `json:"city"`
		State        string `json:"state"`
		Country      string `json:"country"`
	}
	Geolocation struct {
		Latitude  float32 `json:"latitude"`
		Longitude float32 `json:"longitude"`
	} `json:"geolocation"`
	ConsentGrantSkippable bool `json:"constentGrantSkippable"`
}

type Weather struct {
	SolarIntensity struct {
		Type       string  `json:"type"`
		Percentage float32 `json:"percentage"`
		Timestamp  string  `json:"timestamp"`
	}
	OutsideTemperature struct {
		Celsius    float32 `json:"celsius"`
		Fahrenheit float32 `json:"fahrenheit"`
		Timestamp  string  `json:"timestamp"`
		Type       string  `json:"type"`
		Precision  struct {
			Celsius    float32 `json:"celsius"`
			Fahrenheit float32 `json:"fahrenheit"`
		}
	}
	WeatherState struct {
		Type      string `json:"type"`
		Value     string `json:"value"`
		Timestamp string `json:"timestamp"`
	}
}

type Device struct {
	DeviceType       string `json:"deviceType"`
	SerialNo         string `json:"serialNo"`
	ShortSerialNo    string `json:"shortSerialNo"`
	CurrentFwVersion string `json:"currentFwVersion"`
	ConnectionState  struct {
		Value     bool   `json:"value"`
		Timestamp string `json:"timestamp"`
	} `json:"connectionState"`
	Characteristics struct {
		Capabilities []string `json:"capabilities"`
	} `json:"characteristics"`
	InPairingMode    bool     `json:"inPairingMode"`
	ChildLockEnabled bool     `json:"childLockEnabled"`
	BatteryState     string   `json:"batteryState"`
	Duties           []string `json:"duties"`
}

type State struct {
	Presence       string `json:"presence"`
	PresenceLocked bool   `json:"presenceLocked"`
}

type Zone struct {
	Id              int      `json:"id"`
	Name            string   `json:"name"`
	Type            string   `json:"type"`
	DateCreated     string   `json:"dateCreated"`
	DeviceTypes     []string `json:"deviceTypes"`
	Devices         []Device `json:"devices"`
	ReportAvailable bool     `json:"reportAvailable"`
	SupportsDazzle  bool     `json:"supportsDazzle"`
	DazzleEnabled   bool     `json:"dazzleEnabled"`
	DazzleMode      struct {
		Supported bool `json:"supported"`
		Enabled   bool `json:"enabled"`
	} `json:"dazzleMode"`
	OpenWindowDetection struct {
		Supported        bool `json:"supported"`
		Enabled          bool `json:"enabled"`
		TimeoutInSeconds int  `json:"timeoutInSeconds"`
	} `json:"openWindowDetection"`
}

type ZoneState struct {
	TadoMode                       string `json:"tadoMode"`
	GeolocationOverride            bool   `json:"geolocationOverride"`
	GeolocationOverrideDisableTime string `json:"geolocationOverrideDisableTime"`
	Preparation                    string `json:"preparation"`
	Setting                        struct {
		Type        string      `json:"type"`
		Power       string      `json:"power"`
		Temperature Temperature `json:"temperature"`
	} `json:"setting"`
	OverlayType string `json:"overlayType"`
	Overlay     struct {
		Type        string      `json:"type"`
		Setting     TempSetting `json:"setting"`
		Termination struct {
			Type                   string `json:"type"`
			TypeSkillBasedApp      string `json:"typeSkillBasedApp"`
			DurationInSeconds      int    `json:"durationInSeconds"`
			Expiry                 string `json:"expiry"`
			RemainingTimeInSeconds int    `json:"remainingTimeInSeconds"`
			ProjectedExpiry        string `json:"projectedExpiry"`
		} `json:"termination"`
	} `json:"overlay"`
	OpenWindow         bool `json:"openWindow"`
	NextScheduleChange struct {
		Start   string      `json:"start"`
		Setting TempSetting `json:"setting"`
	} `json:"nextScheduleChange"`
	NextTimeBlock struct {
		Start string `json:"start"`
	} `json:"nextTimeBlock"`
	Link struct {
		State string `json:"state"`
	} `json:"link"`
	ActivityDataPoints struct {
		HeatingPower struct {
			Type       string  `json:"type"`
			Percentage float32 `json:"percentage"`
			Timestamp  string  `json:"timestamp"`
		} `json:"heatingPower"`
	} `json:"activityDataPoints"`
	SensorDataPoints struct {
		InsideTemperature struct {
			Celsius    float32 `json:"celsius"`
			Fahrenheit float32 `json:"fahrenheit"`
			Timestamp  string  `json:"timestamp"`
			Type       string  `json:"type"`
			Precision  struct {
				Celsius    float32 `json:"celsius"`
				Fahrenheit float32 `json:"fahrenheit"`
			} `json:"precision"`
		} `json:"insideTemperature"`
		Humidity struct {
			Type       string  `json:"type"`
			Percentage float32 `json:"percentage"`
			Timestamp  string  `json:"timestamp"`
		} `json:"humidity"`
	} `json:"sensorDataPoints"`
}

type ZoneOverlay struct {
	Type    string `json:"type"`
	Setting struct {
		Type        string      `json:"type"`
		Power       string      `json:"power"`
		Temperature Temperature `json:"temperature"`
	} `json:"setting"`
	Termination struct {
		Type                   string `json:"type"`
		Typeskillbasedapp      string `json:"typeSkillBasedApp"`
		DurationInSeconds      int    `json:"durationInSeconds"`
		Expiry                 string `json:"expiry"`
		Remainingtimeinseconds int    `json:"remainingTimeInSeconds"`
		Projectedexpiry        string `json:"projectedExpiry"`
	} `json:"termination"`
}

// GetToken uses the input username and password to retrieve a Token.
// It returns a Token and any error encountered to retrieve it.
func GetToken(username string, password string) (*oauth2.Token, error) {
	// FIXME: Pull client_secret from https://my.tado.com/webapp/env.js

	form := url.Values{
		"client_id":     {ClientId},
		"client_secret": {ClientSecret},
		"grant_type":    {"password"},
		"password":      {password},
		"scope":         {"home.user"},
		"username":      {username},
	}

	resp, err := http.PostForm(TokenUrl, form)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Failed to get token: %v", resp.StatusCode)
	}

	var token *oauth2.Token
	err = json.NewDecoder(resp.Body).Decode(&token)
	if err != nil {
		return nil, fmt.Errorf("Error decoding token: %s", err)
	}

	return token, nil
}

// NewClient gets a new Oauth2 token using username and password and returns a new Tado Client
func NewClient(username string, password string) (*Client, error) {
	var (
		Oauth2Conf = oauth2.Config{
			ClientID:     "public-api-preview",
			ClientSecret: "4HJGRffVR8xb3XdEUQpjgZ1VplJi6Xgw",
			Endpoint: oauth2.Endpoint{
				TokenURL: TokenUrl,
			},
		}
	)

	token, err := GetToken(username, password)
	if err != nil {
		return nil, err
	}
	client := Oauth2Conf.Client(context.Background(), token)
	tadoClient := Client{username, password, Oauth2Conf, client, 0}

	_, err = tadoClient.Account()
	return &tadoClient, err
}

// FIXME
// func (c *Client) RefreshToken() error {}

// getEndpointAndDecode GETs the url and decodes the response into target
func (c *Client) getEndpointAndDecode(url string, target interface{}) error {
	resp, err := c.Client.Get(url)
	if err != nil {
		return fmt.Errorf("Error getting %v: %v", url, err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP Error from %v: %v", url, resp.Status)
	}
	defer resp.Body.Close()

	// body, _ := ioutil.ReadAll(resp.Body)
	// log.Println(string(body))
	// err = json.Unmarshal(body, &target)
	err = json.NewDecoder(resp.Body).Decode(&target)
	if err != nil {
		return fmt.Errorf("Error decoding into %T: %v", resp.Body, err)
	}

	return nil
}

func (c *Client) putEndpointJson(url string, data []byte, target interface{}) error {
	req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("Error creating request: %v", err)
	}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, err := c.Client.Do(req)
	if err != nil {
		return fmt.Errorf("Error sending request %v: %v", req, err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP Error from %v: %v", url, resp.Status)
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&target)
	if err != nil {
		return fmt.Errorf("Error decoding into %T: %v", resp.Body, err)
	}

	return nil
}

// Account return Account which contains additional account details, including the homeId
// Sets the ID of the first Home as the default HomeID in the Client
func (c *Client) Account() (*Account, error) {
	var account Account
	url := fmt.Sprintf("%v/me", ApiUrl)
	err := c.getEndpointAndDecode(url, &account)

	c.HomeId = account.Homes[0].ID
	log.Printf("HomeId set to %v\n", c.HomeId)
	return &account, err
}

// HomeDetails returns HomeDetails for homeId
func (c *Client) HomeDetails() (*HomeDetails, error) {
	var homeDetails HomeDetails
	url := fmt.Sprintf("%v/homes/%v", ApiUrl, c.HomeId)
	return &homeDetails, c.getEndpointAndDecode(url, &homeDetails)
}

// Weather returns Weather for homeId
func (c *Client) Weather() (*Weather, error) {
	var weather Weather
	url := fmt.Sprintf("%v/homes/%v/weather", ApiUrl, c.HomeId)
	return &weather, c.getEndpointAndDecode(url, &weather)
}

func (c *Client) Devices() (*[]Device, error) {
	var devices []Device
	url := fmt.Sprintf("%v/homes/%v/devices", ApiUrl, c.HomeId)
	return &devices, c.getEndpointAndDecode(url, &devices)
}

func (c *Client) State() (*State, error) {
	var state State
	url := fmt.Sprintf("%v/homes/%v/state", ApiUrl, c.HomeId)
	return &state, c.getEndpointAndDecode(url, &state)
}

func (c *Client) Zone() (*[]Zone, error) {
	var zones []Zone
	url := fmt.Sprintf("%v/homes/%v/zones", ApiUrl, c.HomeId)
	err := c.getEndpointAndDecode(url, &zones)
	if err != nil {
		return nil, err
	}
	sort.Slice(zones, func(i, j int) bool {
		return zones[i].Id < zones[j].Id
	})
	return &zones, nil
}

func (c *Client) ZoneState(zoneId string) (*ZoneState, error) {
	var zoneState ZoneState
	url := fmt.Sprintf("%v/homes/%v/zones/%v/state", ApiUrl, c.HomeId, zoneId)
	return &zoneState, c.getEndpointAndDecode(url, &zoneState)
}

func (c *Client) ZoneOverlay(zoneId, targetTemp string, durationSeconds int) (*ZoneOverlay, error) {
	json := fmt.Sprintf(`{"setting": {"type": "HEATING", "power": "ON", "temperature": { "celsius": %v }}, `, targetTemp)
	if durationSeconds == 0 {
		json = json + `"termination": { "type": "TADO_MODE" }}`
	} else {
		json = json + fmt.Sprintf(`"termination": { "type": "TIMER", "durationInSeconds": %v }}`, durationSeconds)
	}

	var zoneOverlayIn = []byte(json)
	var zoneOverlayOut ZoneOverlay
	url := fmt.Sprintf("%v/homes/%v/zones/%v/overlay", ApiUrl, c.HomeId, zoneId)
	return &zoneOverlayOut, c.putEndpointJson(url, zoneOverlayIn, &zoneOverlayOut)
}
