# go-tado

Work in progress

A Golang libary and CLI for the Tado API.

### TODO

* zone list --long, shows state for each zone
* -o/--output json
* Debug 
* Unit tests
* Build CLI
* Auth token rotation
* License

### Examples
* https://github.com/easimon/tado-exporter (Java)
* https://github.com/vide/tado-exporter (Python)
* https://github.com/ekeih/libtado (Python)
* https://gitlab.com/omame/tado-exporter (Go)

API References:
* https://shkspr.mobi/blog/2019/02/tado-api-guide-updated-for-2019/
* http://blog.scphillips.com/posts/2017/01/the-tado-api-v2/
* https://github.com/openhab/openhab-addons/blob/main/bundles/org.openhab.binding.tado/src/main/api/tado-api.yaml

###  Client Secret

https://my.tado.com/webapp/env.js

```
clientSecret: 'wZaRN7rpjn3FoNyF5IFuxg9uMzYJcvOoQ8QWiIqS3hfk6gLhVlG57j5YNoZL2Rtc'
```


### Usage

```
export TADO_USERNAME=username@mail.com
export TADO_PASSWORD=mypassword

go run tado/main.go
```


### Endpoints

* POST /mobile/1.6/getTemperaturePlotData -d fromDate=2017-02-04T23:42:00.012Z -d toDate=2017-02-06T00:17:59.047Z -d zoneId=1
* GET /api/v2/me
* GET /api/v2/homes/<homeID>
* GET /api/v2/homes/<homeID>/weather
