module github.com/dvx76/go-tado

go 1.15

require (
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
	golang.org/x/oauth2 v0.0.0-20210311163135-5366d9dc1934
)
